/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab02;

/**
 *
 * @author ADMIN
 */

import java.util.Scanner;
public class Lab02 {
    
    private static char[][] board = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private static char player = 'X'; 
    
    private static void printWelcome(){
        System.out.println("Welcome to XO Game");
    }
    
    private static void printBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
 
    }
    
    private static void inputRowCol(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Player " + player + " turn : ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        checkBoxEmpty(row,col);
    }
    
    private static void checkBoxEmpty(int row,int col){
        if(row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-'){
                board[row][col] = player;
                switchPlayer();
    }   else {
            System.out.println("Invalid move. Try again.");
        }
        
    }
    
    private static boolean isWinner(){
//        for(int row = 0; row < 3; row++){
//            for(int col = 0; col < 3; col++){
//                if((board[row][0] == board[row][1] && board[row][1] == board[row][2]) || (board[0][col] == board[1][col] && board[1][col] == board[2][col]) || ( row == col && board[0][0] == board[1][1] && board[1][1] == board[2][2]) || ( row + col == 2 && board[2][0] == board[1][1] && board[1][1] == board[0][2])){  
//                    return true;
//                }
//            }
//        }
//        return false;
            for (int i = 0; i < 3; i++) {
                    if (board[i][0] != '-' && board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                        return true;
                    }
                }
            for (int j = 0; j < 3; j++) {
                    if (board[0][j] != '-' && board[0][j] == board[1][j] && board[1][j] == board[2][j]) {
                        return true;
                    }
                }

            // Check diagonals
            if (board[0][0] != '-' && board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
                return true;
            }

            else if (board[0][2] != '-' && board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
                return true;
            }
            return false;
    }
    
    private static void switchPlayer() {
        if(player == 'X'){
            player = 'O';
        }else {
            player = 'X';
        }
    }
    
    private static boolean isDraw() {
        for(int i = 0;i < 3; i++){
                    for(int j = 0;j < 3; j++){
                        if(board[i][j] == '-'){
                            return false;
                        }        
                    }
                }
        return true;
    }
        
    public static void main(String[] args) {
        printWelcome();
        while(true) {
            printBoard();
            inputRowCol();
            if(isWinner()){
                printBoard();
                switchPlayer();
                System.out.println("Player " + player + " WIN");
                break;
            } 
            else if(isDraw()){
                printBoard();
                System.out.println("It’s a draw!");
                break;
            }
        }
    }
}
